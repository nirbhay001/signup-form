import React, { Component } from "react";
import validator from "validator";

class SignUpForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      password: "",
      passwordValid: true,
      repeatPassword: "",
      repeatPasswordValid: true,
      firstName: "",
      firstNameValid: true,
      lastName: "",
      lastNameValid: true,
      email: "",
      emailValid: true,
      age: "",
      ageValid: true,
      formValid: false,
    };
  }
  handleChange = (event) => {
    const value = event.target.value;
    const name = event.target.name;
    this.setState({ ...this.state, [name]: value });
    switch (name) {
      case "firstName":
        {
          validator.isAlpha(value) && value.length >= 2
            ? this.setState({ ...this.state, firstNameValid: true })
            : this.setState({ ...this.state, firstNameValid: false });
        }
        break;
      case "lastName":
        {
          validator.isAlpha(value) && value.length >= 2
            ? this.setState({ ...this.state, lastNameValid: true })
            : this.setState({ ...this.state, lastNameValid: false });
        }
        break;
      case "age":
        {
          validator.isNumeric(value) && !value.includes("e")
            ? this.setState({ ...this.state, ageValid: true })
            : this.setState({ ...this.state, ageValid: false });
        }
        break;
      case "email":
        {
          validator.isEmail(value)
            ? this.setState({ ...this.state, emailValid: true })
            : this.setState({ ...this.state, emailValid: false });
        }
        break;
      case "password":
        {
          validator.isStrongPassword(value)
            ? this.setState({ ...this.state, passwordValid: true })
            : this.setState({ ...this.state, passwordValid: false });
        }
        break;
      case "repeatPassword": {
        validator.isStrongPassword(value)
          ? this.setState({ ...this.state, repeatPasswordValid: true })
          : this.setState({ ...this.state, repeatPasswordValid: false });
      }
      break;
      default:
    }
  };

  handleSubmit = (event) => {
    event.preventDefault();
    if (
      this.state.passwordValid &&
      this.state.repeatPasswordValid &&
      this.state.firstNameValid &&
      this.state.ageValid &&
      this.state.lastNameValid &&
      this.state.emailValid
    ) {
      this.setState({
        ...this.state,
        formValid: true,
      });
    }
  };

  render() {
    return this.state.formValid ? (
      <h1 className="text-center">You have submitted form successfully</h1>)
      : (
      <div className="container-sm mb-4 ">
        <h2 className="bg-dark text-white p-3 mt-3">Sign Up Form</h2>
        <form action="#" method="post" onSubmit={this.handleSubmit}>
          <div className="form-group ">
            <i className="fas fa-user pl-2"></i>
            <label htmlFor="firstName" className="pl-2">
              First Name:
            </label>
            <input
              type="text"
              className="form-control"
              name="firstName"
              placeholder="Enter First Name..."
              onChange={this.handleChange}
              required
            />
            {this.state.firstNameValid ? (
              <p className="invisible">""</p>
            ) : (
              <p className="text-danger">Please, Enter valid name...</p>
            )}
          </div>
          <div className="form-group mt-n3">
            <i className="fas fa-user pl-2"></i>
            <label htmlFor="lastName" className="pl-2">
              Last Name:
            </label>
            <input
              type="text"
              className="form-control"
              name="lastName"
              onChange={this.handleChange}
              placeholder="Enter Last Name..."
              required
            />
            {this.state.lastNameValid ? (
              <p className="invisible">""</p>
            ) : (
              <p className="text-danger">Please, Enter valid name...</p>
            )}
          </div>
          <div className="form-group mt-n3">
            <label htmlFor="age">Age:</label>
            <input
              type="number"
              className="form-control"
              name="age"
              placeholder="Enter Age..."
              min={18}
              max={100}
              onChange={this.handleChange}
              required
            />
            {this.state.ageValid ? (
              <p className="invisible">""</p>
            ) : (
              <p className="text-danger">Please, Enter age in number...</p>
            )}
          </div>
          <div className="form-group mt-n3">
            <i className="fa-solid fa-venus-mars pl-2"></i>
            <label htmlFor="gender" className="pl-2">
              Gender:
            </label>
            <select className="form-control" name="gender" required>
              <option value="">Select Gender</option>
              <option value="male">Male</option>
              <option value="female">Female</option>
              <option value="other">Other</option>
            </select>
          </div>
          <div className="form-group">
            <i className="fa-solid fa-envelope pl-2"></i>
            <label htmlFor="email" className="pl-2">
              Email:
            </label>
            <input
              type="email"
              className="form-control"
              name="email"
              placeholder="Enter Email Adress..."
              onChange={this.handleChange}
              required
            />
            {this.state.emailValid ? (
              <p className="invisible">""</p>
            ) : (
              <p className="text-danger">Please enter valid email adress..</p>
            )}
          </div>
          <div className="form-group mt-n3">
            <i className="fa-solid fa-rocket pl-2"></i>
            <label htmlFor="role" className="pl-2">
              Role:
            </label>
            <select className="form-control" id="role" name="role" required>
              Developer/Senior Developer/Lead Engineer/CTO
              <option value="">Select Role</option>
              <option value="student">Developer</option>
              <option value="teacher">Senior Developer</option>
              <option value="parent">Lead Engineer</option>
              <option value="parent">Lead CTO</option>
            </select>
          </div>
          <div className="form-group">
            <i className="fa-solid fa-key pl-2"></i>
            <label htmlFor="password" className="pl-2">
              Password:
            </label>
            <input
              type="password"
              className="form-control"
              name="password"
              placeholder="Enter password..."
              onChange={this.handleChange}
              required
            />
            {this.state.passwordValid ? (
              <p className="invisible">""</p>
            ) : (
              <div className="text-danger">
                Password must be at least 8 characters long, contain at least
                one uppercase letter, one lowercase letter, one digit, and one
                symbol.
              </div>
            )}
          </div>

          <div className="form-group mt-n3">
            <i className="fa-solid fa-key pl-2"></i>
            <label htmlFor="repeatPassword" className="pl-2">
              Repeat Password:
            </label>
            <input
              type="password"
              className="form-control"
              name="repeatPassword"
              placeholder="Enter password..."
              onChange={this.handleChange}
              required
            />
            {this.state.repeatPasswordValid ? (
              <p className="invisible">""</p>
            ) : (
              <div className="text-danger">
                Password must be at least 8 characters long, contain at least
                one uppercase letter, one lowercase letter, one digit, and one
                symbol.
              </div>
            )}
          </div>
          <div className="form-check mt-n3">
            <input
              type="checkbox"
              className="form-check-input"
              name="terms"
              required
            />
            <label className="form-check-label" htmlFor="terms">
              I agree to the <a href="#">Terms and Conditions</a>
            </label>
          </div>
          <button
            type="submit"
            className="btn btn-primary btn-lg btn-block mt-3"
          >
            Sign Up
          </button>
        </form>
      </div>
    );
  }
}
export default SignUpForm;
